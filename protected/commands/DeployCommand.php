<?php
class DeployCommand extends CConsoleCommand
{
	public $local=false;
	public $skip_db_clear=false;

	public function actionIndex()
	{
		// If you want to refresh only one table, you can also do :
		// Yii::app()->db->schema->getTable('tablename', true);

		if (!$this->skip_db_clear) {
			// Load all tables of the application in the schema
			Yii::app()->db->schema->getTables();

			// clear the cache of all loaded tables
			Yii::app()->db->schema->refresh();

			echo 'Clear sql schema cache complete!'."\n";
		}

		// Если были изменения в файлах ресурсов (js, css), то обновим дату модификации контрольного файла.
		$versionFile = Yii::app()->getRuntimePath().DIRECTORY_SEPARATOR.Yii::app()->params['resourcesVersionFilename'];
		$curentVersionValue = $this->getResourcesVersion();

		if (file_exists($versionFile)) {
			$fileVersionValue = file_get_contents($versionFile);
			if ($fileVersionValue != $curentVersionValue) {
				$this->modifyVersionInFile($versionFile, $curentVersionValue);
			}
		} else {
			$this->modifyVersionInFile($versionFile, $curentVersionValue);
		}

	}

	private function getResourcesVersion()
	{
		$css = Yii::getPathOfAlias('webroot.media.css');
		$cssFiles = CFileHelper::findFiles($css, array('fileTypes'=>array('css')));

		$js = Yii::getPathOfAlias('webroot.media.js');
		$jsFiles = CFileHelper::findFiles($js, array('fileTypes'=>array('js')));

		$files = array();
		if (!empty($cssFiles)) {
			$files = CMap::mergeArray($files, $cssFiles);
		}
		if (!empty($jsFiles)) {
			$files = CMap::mergeArray($files, $jsFiles);
		}

		$stamps = array();
		if (!empty($files)) {
			foreach($files as $file) {
				$stamps[] = filemtime($file);
			}
		}

		return md5(implode('-', $stamps));
	}

	private function modifyVersionInFile($versionFile, $curentVersionValue)
	{
		file_put_contents($versionFile, $curentVersionValue);
		echo 'Resources version file modified!'."\n";
	}
}
