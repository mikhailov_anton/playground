<div class="app-site-restore-expired">
	<p>Link expired. Forgot password links expire within <?php echo Yii::t('app', '{n} day|{n} days', UserIdentityKeys::FORGONT_PASSWORD_TTL) ?></p>
	<p>Go to "<a href="<?php echo $this->createUrl('/site/forgot');?>">Forgor password page</a>"</p>
	<p>Type your email address and click "Continue". System will send you new email</p>
	<p>Go to your inbox and click the link inside email.</p>
</div>