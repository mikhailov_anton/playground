<?php
$this->metaTitle = 'Udimi. Forgot password';
?>
<div class="app-site-auth">
	<div class="b-title">
		<div class="b-col-left">
			Forgot password
		</div>
		<div class="b-col-right">
			<a href="<?php echo $this->createUrl(Yii::app()->user->loginUrl) ?>" class="ajax-get">Cancel</a>
		</div>
	</div>


	<div class="e-line"></div>

	<?php $form = $this->beginWidget(
		'CActiveForm', array(
			'id' => 'forgot-form',
			'htmlOptions' => array(
				'class' => 'form-horizontal',
			),
		)
	); ?>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'username', array('class'=>'control-label col-sm-3 required-hide')); ?>
		<div class="col-sm-9">
			<?php echo $form->textField($model, 'username', array('class'=>'form-control')); ?>
			<div class="e-err"><?= $model->getError('username') ?></div>
		</div>
	</div>

	<div class="b-note">
		Please enter the Email, which you used to access the site.
	</div>

	<div class="b-btn">
		<button type="submit" class="btn btn-modern-primary ajax-post">Continue</button>
	</div>

	<?php $this->endWidget(); ?>

</div>