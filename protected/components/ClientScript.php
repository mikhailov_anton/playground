<?php

class ClientScript extends CClientScript
{
	public function render(&$output)
	{
		if(!$this->hasScripts)
			return;

		if(!empty($this->scriptMap))
			$this->remapScripts();

		$this->unifyScripts();

		$this->renderHead($output);
		if($this->enableJavaScript)
		{
			$this->renderBodyBegin($output);
			$this->renderBodyEnd($output);
		}
	}
}