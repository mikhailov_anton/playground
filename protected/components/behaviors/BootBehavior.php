<?php
class BootBehavior extends CBehavior
{
	public function events()
	{
		return array_merge(parent::events(), array(
			'onBeginRequest' => 'beginRequest',
		));
	}

	public function beginRequest($event)
	{
		if (!isset(Yii::app()->params['default_time_zone'])) return;
		date_default_timezone_set(Yii::app()->params['default_time_zone']);

		$now = new DateTime();
		$mins = $now->getOffset() / 60;
		$sgn = $mins < 0 ? -1:1;
		$mins = abs($mins);
		$hrs = floor($mins / 60);
		$mins -= $hrs * 60;

		Yii::app()->db->createCommand("SET @@session.time_zone = '".sprintf('%+d:%02d', $hrs*$sgn, $mins)."'")->execute();

		unset($_GET['_']);
	}
}