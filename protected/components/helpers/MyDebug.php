<?php

class MyDebug {
	public static function traceStart($name=null)
	{
		if (empty(Yii::app()->params['is_local'])) return;
		if (!$name) $name = 'trace';
		xdebug_start_trace(Yii::app()->basePath.'/logs/'.$name);
	}

	public static function traceStop()
	{
		xdebug_stop_trace();
	}
}