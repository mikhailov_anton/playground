<?php
class MyString
{
	// Выполняет замену только если строка не html и делает это 1 раз.
	// Т.е. повторный запуск ничего не сломает в плане переносов строк.
	public static function nl2brHuman($text)
	{
		if ($text == MyString::strip_tags_smart($text)) {
			// plain text (NOT html)
			return nl2br($text);
		}
		return $text;
	}

	/**
	 * допустимые символы
	 * @param string $value
	 * @return string
	 */
	public static function govnoStrip($value)
	{
		return trim(preg_replace('/[^\w\d\s\-,`~_!<>@#&%"\';:\Q[]\/^$.|?*+(){}\E]+/', '', $value));
	}

	/**
	 * Убирает более одного пробела в строке.
	 * @param $value
	 *
	 * @return string
	 */
	public static function spaceStrip($value)
	{
		return preg_replace('/\s\s+/', ' ', $value);
	}

	/**
	 * Привалить переносы строк
	 * @param $value - string
	 *
	 * @return string
	 */
	public static function nlStrip($value){
		return str_replace(array("\r", "\n"), array(" "), $value);
	}

	public static function formatPrice($price){
		return trim(str_replace(",", ".", $price));
	}

	/**
	 * умное удаление текгов из строки
	 * @param string $s - строка для обработки
	 * @param array $allowable_tags
	 * @param bool $is_format_spaces
	 * @param array $pair_tags
	 * @param array $para_tags
	 * @return array|mixed|string
	 */
	public static function strip_tags_smart(
		/*string*/ $s,
		array $allowable_tags = null,
		/*boolean*/ $is_format_spaces = true,
		array $pair_tags = array('script', 'style', 'map', 'iframe', 'frameset', 'object', 'applet', 'comment', 'button'),
		array $para_tags = array('p', 'td', 'th', 'li', 'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'div', 'form', 'title', 'pre', 'textarea')
	)
	{
		//return strip_tags($s);
		static $_callback_type  = false;
		static $_allowable_tags = array();
		static $_para_tags      = array();
		#регулярное выражение для атрибутов тагов
		#корректно обрабатывает грязный и битый HTML в однобайтовой или UTF-8 кодировке!
		static $re_attrs_fast_safe =  '(?![a-zA-Z\d])  #утверждение, которое следует сразу после тага
	                                   #правильные атрибуты
	                                   (?>
	                                       [^>"\']+
	                                     | (?<=[\=\x20\r\n\t]|\xc2\xa0) "[^"]*"
	                                     | (?<=[\=\x20\r\n\t]|\xc2\xa0) \'[^\']*\'
	                                   )*
	                                   #разбитые атрибуты
	                                   [^>]*';

		if (is_array($s))
		{
			if ($_callback_type === 'strip_tags')
			{
				$tag = strtolower($s[1]);
				if ($_allowable_tags &&
					(array_key_exists($tag, $_allowable_tags) || array_key_exists('<' . trim(strtolower($s[0]), '< />') . '>', $_allowable_tags))
				) return $s[0];
				if ($tag == 'br') return "\r\n";
				if ($_para_tags && array_key_exists($tag, $_para_tags)) return "\r\n\r\n";
				return '';
			}
			if ($_callback_type === 'strip_spaces')
			{
				if ($s[0]{0} === '<') return $s[0];
				return ' ';
			}
			trigger_error('Unknown callback type "' . $_callback_type . '"!', E_USER_ERROR);
		}

		if (($pos = strpos($s, '<')) === false || strpos($s, '>', $pos) === false)  #оптимизация скорости
		{
			#таги не найдены
			return $s;
		}

		$length = strlen($s);

		#непарные таги (открывающие, закрывающие, !DOCTYPE, MS Word namespace)
		$re_tags = '/<[\/\!]?+ ([a-zA-Z][a-zA-Z\d]*+ (?>\:[a-zA-Z][a-zA-Z\d]*)?)' . $re_attrs_fast_safe . '>/sxS';

		$patterns = array(
			'/<([\?\%]) .*? \\1>/sxS',     #встроенный PHP, Perl, ASP код
			'/<\!\[CDATA\[ .*? \]\]>/sxS', #блоки CDATA
			#'/<\!\[  [\x20\r\n\t]* [a-zA-Z] .*?  \]>/sxS',  #:DEPRECATED: MS Word таги типа <![if! vml]>...<![endif]>

			'/<\!--.*?-->/sS', #комментарии

			#MS Word таги типа "<![if! vml]>...<![endif]>",
			#условное выполнение кода для IE типа "<!--[if expression]> HTML <![endif]-->"
			#условное выполнение кода для IE типа "<![if expression]> HTML <![endif]>"
			#см. http://www.tigir.com/comments.htm
			'/<\! (?:--)?+
	              \[
	              (?> [^\]"\']+ | "[^"]*" | \'[^\']*\' )*
	              \]
	              (?:--)?+
	         >/sxS',
		);
		if ($pair_tags)
		{
			#парные таги вместе с содержимым:
			foreach ($pair_tags as $k => $v) $pair_tags[$k] = preg_quote($v, '/');
			$patterns[] = '/<((?i:' . implode('|', $pair_tags) . '))' . $re_attrs_fast_safe . '> .*? <\/(?i:\\1)' . $re_attrs_fast_safe . '>/sxS';
		}
		#d($patterns);

		$i = 0; #защита от зацикливания
		$max = 99;
		while ($i < $max)
		{
			$s2 = preg_replace($patterns, '', $s);
			if (preg_last_error() !== PREG_NO_ERROR)
			{
				$i = 999;
				break;
			}

			if ($i == 0)
			{
				$is_html = ($s2 != $s || preg_match($re_tags, $s2));
				if (preg_last_error() !== PREG_NO_ERROR)
				{
					$i = 999;
					break;
				}
				if ($is_html)
				{
					if ($is_format_spaces)
					{
						#В библиотеке PCRE для PHP \s - это любой пробельный символ, а именно класс символов [\x09\x0a\x0c\x0d\x20\xa0] или, по другому, [\t\n\f\r \xa0]
						#Если \s используется с модификатором /u, то \s трактуется как [\x09\x0a\x0c\x0d\x20]
						#Браузер не делает различия между пробельными символами,
						#друг за другом подряд идущие символы воспринимаются как один
						#$s2 = str_replace(array("\r", "\n", "\t"), ' ', $s2);
						#$s2 = strtr($s2, "\x09\x0a\x0c\x0d", '    ');
						$_callback_type = 'strip_spaces';
						$s2 = preg_replace_callback('/  [\x09\x0a\x0c\x0d]++
	                                                  | <((?i:pre|textarea))' . $re_attrs_fast_safe . '>
	                                                    .+?
	                                                    <\/(?i:\\1)' . $re_attrs_fast_safe . '>
	                                                 /sxS', array('self', __FUNCTION__), $s2);
						$_callback_type = false;
						if (preg_last_error() !== PREG_NO_ERROR)
						{
							$i = 999;
							break;
						}
					}

					#массив тагов, которые не будут вырезаны
					if ($allowable_tags) $_allowable_tags = array_flip($allowable_tags);

					#парные таги, которые будут восприниматься как параграфы
					if ($para_tags) $_para_tags = array_flip($para_tags);
				}
			}#if

			#обработка тагов
			if ($is_html)
			{
				$_callback_type = 'strip_tags';
				$s2 = preg_replace_callback($re_tags, array('self', __FUNCTION__), $s2);
				$_callback_type = false;
				if (preg_last_error() !== PREG_NO_ERROR)
				{
					$i = 999;
					break;
				}
			}

			if ($s === $s2) break;
			$s = $s2; $i++;
		}#while
		if ($i >= $max) $s = strip_tags($s); #too many cycles for replace...

		if ($is_format_spaces && strlen($s) !== $length)
		{
			#вырезаем дублирующие пробелы
			$s = preg_replace('/\x20\x20++/sS', ' ', trim($s));
			#вырезаем пробелы в начале и в конце строк
			$s = str_replace(array("\r\n\x20", "\x20\r\n"), "\r\n", $s);
			#заменяем 3 и более переносов строк на 2 переноса строк
			#$s = preg_replace('/\r\n[\r\n]++/sS', "\r\n\r\n", $s);
			$s = preg_replace('/[\r\n]{3,}+/sS', "\r\n\r\n", $s);
		}
		return $s;
	}


	/**
	 * Делает то же, что substr, но не разрывает слова
	 *
	 * @param string $str
	 * @param int    $start
	 * @param int    $length
	 * @param bool   $ellipsis показывать ли многоточие
	 *
	 * @return string
	 */
	public static function substrWords($str, $start, $length, $ellipsis = true)
	{
		if (strlen($str) <= $length) {
			return $str;
		}
		$substr = substr($str, $start, $length);

		$pos = strrpos($substr, ' ');
		$needEllipsis = $ellipsis && strlen(trim($str)) > strlen($substr);
		return substr($substr, 0, $pos) . ($needEllipsis ? '...' : '');
	}

	public static function parseUrl2Html($text, $style='')
	{
		return preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<a '.$style.' href="$1://$2">$1://$2</a>$3', $text);
	}

	// Сохраняет ссылки в формате html и преобразует только текстовые урлы,
	// при этом не трогает текстовые урлы содержащие чужие домены.
	public static function makeClickableLinks($text, $domain=false)
	{
		$d = $domain ? $domain : Yii::app()->params['system_domain'];

		// Замена уже имеющихся html ссылок c подсохранением в массив.
		$GLOBALS['_udimi']['parseUrl2HtmlDomain']['storage'] = [];
		$GLOBALS['_udimi']['parseUrl2HtmlDomain']['i'] = 0;
		$text = preg_replace_callback(
			'/<a(.+?)href=("|\')(.+?)("|\')(.*?)>(.+?)<\/a>/i',
			function ($m) {
				$GLOBALS['_udimi']['parseUrl2HtmlDomain']['i']++;
				$GLOBALS['_udimi']['parseUrl2HtmlDomain']['storage'][$GLOBALS['_udimi']['parseUrl2HtmlDomain']['i']] = $m;
				return '{{{'. $GLOBALS['_udimi']['parseUrl2HtmlDomain']['i'].'}}}';
			},
			$text
		);

		//замена текстовых ссылок
		$text = preg_replace("~(http|https|ftp|ftps)://(blog.)?(".$d.".*?)(\s|\n|<|[,.?!](\s|\n)|$)~", '<a href="$1://$2$3">$1://$2$3</a>$4', $text);

		// возврат ранее сохраненных html ссылок
		if (!empty($GLOBALS['_udimi']['parseUrl2HtmlDomain']['storage'])) {
			foreach ($GLOBALS['_udimi']['parseUrl2HtmlDomain']['storage'] as $key=>$val) {
				$text = str_replace('{{{'.$key.'}}}', '<a'.$val[1].'href='.$val[2].$val[3].$val[4].$val[5].'>'.$val[6].'</a>', $text);
			}
		}

		return $text;
	}

	public static function replaceLinkHref($text, $link)
	{
		return preg_replace('/<a(.+?)href=("|\')(.+?)("|\')(.*?)>(.+?)<\/a>/i', '<a$1href="'.$link.'"$5>$6</a>', $text);
	}

	public static function isContainLink($text)
	{
		$result = false;
		$text = str_replace(['http://', 'https://'], [' http://', ' https://'], $text);
		$arr = explode(" ", $text);
		foreach($arr as $word) {
			$word = trim($word);
			$result = preg_match_all('/^(http:\/\/|https:\/\/|www.)(([A-Z0-9][A-Z0-9_-]*)(\.[A-Z0-9][A-Z0-9_-]*)+)/i', $word);
			if ($result) break;
		}
		return $result;
	}
}