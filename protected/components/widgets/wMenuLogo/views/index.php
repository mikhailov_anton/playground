<div class="app-widgets-menu-logo js-appmain-menu-depend-marker">
	<ul>
		<?php foreach($items as $item): ?>
			<?php if ($item->attributes['visible']): ?>
				<?= CHtml::openTag('li', ['class'=>$item->attributes['liClass']]) ?>

					<?= CHtml::openTag('a', array_merge($item->attributes['htmlOptions'], ['href'=>$item->attributes['url']])) ?>

					<span><?= $item->attributes['label'] ?></span>

					<?= CHtml::closeTag('a') ?>

				<?= CHtml::closeTag('li') ?>
			<?php endif ?>
		<?php endforeach ?>
	</ul>
</div>