function AppWidgetSimpleDrop(selector) {

	this.$dropdown = typeof selector == 'object' ? selector : $(selector);
	this.clLbl = '.js-lbl';
	this.clDropmenu = '.js-dropmenu';
	this.input;

	this.onSelectCallback;

	var self = this;

	this.init = function()
	{
		var clOpen = '.m-open',
			clBackdrop = '.e-backdrop',
			clCaretDown = '.fa-caret-down',
			clCaretUp = '.fa-caret-up';

		this.$input = this.$dropdown.find('input[type=hidden]');
		this.hideCaret = !self.$dropdown.find(self.clLbl).find('i');

		// Событие открытия/закрытия дропа
		self.$dropdown.click(function() {

			var $this = $(this);
			_clOpen = clOpen.slice(1);
			_clBackdrop = clBackdrop.slice(1);
			_clCaretDown = clCaretDown.slice(1);
			_clCaretUp = clCaretUp.slice(1);

			if ($this.hasClass(_clOpen)) {
				// Закрыть дроп. Убрать класс open и убрать backdrop
				$this.removeClass(_clOpen);
				$this.find(clBackdrop).remove();
				self.$dropdown.find(self.clLbl+' .fa').removeClass(_clCaretUp).addClass(_clCaretDown);
			} else {
				// Открыть дроп. Добавить класс open и добавить backdrop
				$this.addClass(_clOpen);
				$('<div/>', {class: _clBackdrop}).insertBefore($this.find(self.clDropmenu));
				self.$dropdown.find(self.clLbl+' .fa').removeClass(_clCaretDown).addClass(_clCaretUp);
			}
		});

		// Событие выбора значения в дропе
		self.$dropdown.find(self.clDropmenu+' li').click(function() {
			self.setSelectedClicksDropdown($(this).data('val'));
		});
	};

	this.setSelectedClicksDropdown = function(val, skipCallback)
	{
		var lbl = self.$dropdown.find(self.clDropmenu+' li[data-val='+val+']').html();
		skipCallback = skipCallback || false;

		if (!self.hideCaret) {
			lbl = lbl + '<i class="fa fa-caret-down"></i>';
		}

		self.$dropdown.find(self.clLbl).html(lbl);
		this.$input.val(val);

		if (self.onSelectCallback && !skipCallback) {
			self.onSelectCallback(val, this.$input);
		}
	};

	this.onSelect = function(callback)
	{
		self.onSelectCallback = callback;
	}
}