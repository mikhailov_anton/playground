<?php
return array(
	'class'=>'CLogRouter',
	'routes'=>array(
		array(
			'class'		=> 'CFileLogRoute',
			'levels'	=> 'error',
			'logFile'	=> '404.log',
			'logPath' => dirname(__FILE__).'/../logs/',
			'categories'=> 'exception.CHttpException.404',
			'maxFileSize' => 1024,
			'maxLogFiles' => 5,
		),
		array(
			'class'		=> 'CFileLogRoute',
			'levels'	=> 'error',
			'logFile'	=> 'error.log',
			'logPath' => dirname(__FILE__).'/../logs/',
			'categories'=> 'php, exception.CDbException.*, exception.CException.*, application',
			'maxFileSize' => 1024,
			'maxLogFiles' => 5,
			'filter'	=> array(
				'class'		=>'CLogFilter',
				'logUser' 	=> false,
				'logVars'	=> array(),
			)
		),
		array(
			'class'=>'CFileLogRoute',
			'levels'=>'error, warning',
			'logFile'	=> 'application.log',
			'logPath' => dirname(__FILE__).'/../logs/',
			'except'=>array(
				'monitoring',
				'php',
				'exception.CDbException.*',
				'exception.CException.*',
				'exception.CHttpException.404',
			),
			'maxFileSize' => 1024,
			'maxLogFiles' => 20,
		)
	),
);