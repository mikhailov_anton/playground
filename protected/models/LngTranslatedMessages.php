<?php

class LngTranslatedMessages extends ActiveRecord
{
	public function tableName()
	{
		return 'lng_translated_messages';
	}

	public function relations()
	{
		return array(
//			'idUser' => array(self::BELONGS_TO, 'Users', 'id_user'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
