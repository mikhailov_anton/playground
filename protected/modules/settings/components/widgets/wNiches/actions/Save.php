<?php
class Save extends WidgetBaseAction
{
	public function run()
	{
		if (isset($_POST['nicheUsersPost'])) {
			$widget = $this->getWidgetInstance();
			$model = $widget->model;

			$userIds = $this->getUserIds();
			$postIds = isset($_POST['nicheUsers']) ? $_POST['nicheUsers'] : array();

			foreach ($this->getAllIds($model) as $baseId) {
				if (in_array($baseId, $postIds)) {
					// пришел установленный сb. Нужно добавить запись если она не существует либо ничего не делать
					if (!in_array($baseId, $userIds)) {
						if (sizeof($userIds) >= NicheUsers::NICHES_MAX) {
							$this->controller->jsonResponse(
								array(
									'error' => "You can't select more than ".NicheUsers::NICHES_MAX." niches",
									'callback' => 'settingsSeller.initNiches('.CJavaScript::encode(array('nichesMax'=>NicheUsers::NICHES_MAX)).');',
									'container' => '#'.$widget->containerId,
									'content' => $widget->run(),
								)
							);
						} else {
							$model = new nicheUsers('settings');
							$model->id_niche = $baseId;
							$model->save();
						}
					}

				} else {
					// пришел снятый cb. Нужно удалить запись если она есть либо ничего не делать
					if (in_array($baseId, $userIds)) {
						$model = nicheUsers::model()->curUser()->findByAttributes(array('id_niche'=>$baseId));
						$model->delete();
					}
				}
			}

			// создадим еще раз экземпляр пустой модели и выведем пустую форму
			$widget->init();

			// Response
			$this->controller->jsonResponse(
				array(
					'callback' => '
						appMain.showToast("Changes have been saved", "success");
						settingsSeller.initNiches();
					',
					'container' => '#'.$widget->containerId,
					'content' => $widget->run(),
				)
			);
		}
	}

	private function getUserIds()
	{
		$result = array();
		$model = nicheUsers::model()->curUser()->findAll();
		if (!empty($model)) {
			foreach($model as $item) {
				$result[] = $item->id_niche;
			}
		}
		return $result;
	}

	private function getAllIds($model)
	{
		$result = array();
		foreach ($model as $item) {
			$result[] = $item->id;
		}
		return $result;
	}
}