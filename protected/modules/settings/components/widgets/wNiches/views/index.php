<?php if (!empty($model)): ?>
	<input type="hidden" name="nicheUsersPost" value="1">
	<div class="b-niches">
		<?php foreach ($model as $item): ?>
			<div class="checkbox-inline checkbox checkbox-primary">
				<?= CHtml::checkBox(
					'nicheUsers[]', isset($item->nicheUsers[0]), array(
						'id' => 'nicheUsers_' . $item->id,
						'value' => $item->id,
						'class' => 'niche-item-checkbox',
					)
				) ?>
				<label for="nicheUsers_<?= $item->id ?>"><?= $item->name ?></label>
			</div>
		<?php endforeach ?>
	</div>
	<button type="submit" class="ajax-post hidden" data-href="<?= $this->controller->createUrl('wniches.save') ?>" data-widget="wniches" id="listniches-submit"></button>
<?php endif; ?>