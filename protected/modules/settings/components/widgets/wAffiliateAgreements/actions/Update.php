<?php
class Update extends WidgetBaseAction
{
	public function run()
	{
		$model = new AffiliateAgreements('create-update');

		if (isset($_POST[get_class($model)]))
			$model->attributes = $_POST[get_class($model)];

		$model->validate();

		$this->controller->layoutContainer = '.preview-container';
		$this->controller->appendJsonResponse([	'callback'=>[
			"$('.popover').remove();",
		]]);
		$this->controller->render('application.modules.settings.components.widgets.wAffiliateAgreements.views.partial.partner_item', [
			'user'=>$model->profileUserModel,
			'percent'=>$model->percent,
		]);
	}
}