<?php
class wAffiliateAgreements extends Widget
{
	public function init()
	{
		// Обязательный вызов.
		parent::init(__CLASS__);
	}

	/**
	 * @return string|void Классический метод виджета
	 */
	public function run()
	{
		$profileSolo = Yii::app()->user->model->profileSolos;

		$criteria = AffiliateAgreements::getBaseCriteria(Yii::app()->user->id);
		$models = AffiliateAgreements::model()->findAll($criteria);

		return $this->render('index', [
			'models'=>$models,
			'profileSolo'=>$profileSolo,
		]);
	}

	/**
	 * @return array Регистрируем actions
	 *
	 * Пример регистрации базового класса WidgetBaseAction в качестве экшна перерисовки виджета:
	 * 'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
	 * в яваскрипте доступна функция appMain.ajaxLoadWidgetUrl('wmenuleft', 'reload')
	 * где wmenuleft - имя класса виджета, а reload - имя экшна (ключ массива actions)
	 *
	 * Пример регистрации произвольного класса widget action:
	 * 'example'=>array('class'=>__CLASS__.'.actions.Example', 'widget_alias'=>__CLASS__),
	 * Необходимо создать класс в директории actions в папке виджета: class Example extends WidgetBaseAction
	 * в классе доступен метод получения экземпляра текущего виджета с его параметрами: $this->getWidgetInstance()
	 * в шаблоне обращение к такому action выглядит следующим образом:
	 * <a href="<?php echo $this->createUrl('wmenuleft.example', array('x'=>5)) ?>" data-widget="wmenuleft" class="ajax-get">Load</a>
	 */
	public static function actions()
	{
		Yii::setPathOfAlias(__CLASS__, realpath(dirname(__FILE__)));
		return array(
			'reload'=>array('class'=>'application.components.WidgetBaseAction', 'widget_alias'=>__CLASS__),
			'save'=>array('class'=>__CLASS__.'.actions.Save', 'widget_alias'=>__CLASS__),
			'create'=>array('class'=>__CLASS__.'.actions.Create', 'widget_alias'=>__CLASS__),
			'update'=>array('class'=>__CLASS__.'.actions.Update', 'widget_alias'=>__CLASS__),
			'cancel'=>array('class'=>__CLASS__.'.actions.Cancel', 'widget_alias'=>__CLASS__),
		);
	}
} 