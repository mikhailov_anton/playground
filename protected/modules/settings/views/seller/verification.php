<?php $this->beginContent('/layouts/main'); ?>
	<div class="settings-seller-verification">

		<?php if ($complete): ?>
			<div class="complete-msg">
				<p>This operation is carried out manually by Udimi support. Your list will be verified in 72 hours or less. If we experience a problem verifying your list, you will be contacted by our team via messenger.</p>
				<p>&nbsp;</p>
				<p><button href="<?= $this->createUrl('index') ?>" class="ajax-get btn btn-primary">Continue</button></p>
			</div>

		<?php else: ?>
			<?php if (!empty($appsVerified)): ?>
				<div class="section-complete-verifications">
					<div class="b-title">
						<?php $this->widget('w.wGtitle.wGtitle', array(
							'left'=>'Currently connected',
						)) ?>
					</div>
					<div class="complete-items">
						<?php foreach ($appsVerified as $app): ?>
							<form>
								<div class="item-row">
									<div class="col-1">
										<label for="list_size_<?= $app->list_size ?>"><?= $app->idApp->name ?></label>
									</div>
									<div class="col-2">
										<?= $app->list_size ?>
									</div>
									<div class="col-3">
										<button class="ajax-post btn btn-black" data-href="<?= $this->createUrl('delete', array('id'=>$app->id)) ?>" data-confirm="Are you sure?">Disconnect</button>
									</div>
									<div class="clearfix"></div>
								</div>
							</form>
						<?php endforeach; ?>
						<div class="item-row">
							<div class="col-1">
								<span class="green-color">Total:</span>
							</div>
							<div class="col-total">
								<span class="green-color"><?=AppsUsers::getVerifiedListSize(Yii::app()->user->id) ?> subscribers</span>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			<?php endif ?>

			<div class="section-request">

				<div class="b-title">
					<?php $this->widget('w.wGtitle.wGtitle', array(
						'left'=>'Autoresponders',
					)) ?>
				</div>

				<div class="request-items">

					<?php $form = $this->beginWidget(
						'CActiveForm', array(
						'id' => 'verification-form',
						'htmlOptions' => array(
							'data-init' => 'dirty-check',
							'class' => 'form-horizontal',
						),
					)) ?>
					<?php foreach ($apps as $app):
						$inst = new $app->class(Yii::app()->user->id) ?>
						<div class="item-row radio radio-primary">
							<?= $form->radioButton(
								$model,
								'idsAppRequest[]',
								array(
									'id'=>'cb_id_'.$app->id,
									'value'=>$app->id,
									'uncheckValue'=>0,
									'disabled'=>!$inst->isConnected() || !empty($app->listsizeVerifications)
								)) ?>
							<label for="cb_id_<?= $app->id ?>">
								<span class="<?=($inst->isConnected() ? '':'grey-color') ?>"><?= $app->name ?></span>
							</label>

							<div class="status-info">
								<?php if (!$inst->isConnected()): ?>
									<a href="<?= $this->createUrl('apps/coninit', array('id_app'=>$app->id)) ?>" class="ajax-get">Connect</a>

								<?php elseif (!empty($app->listsizeVerifications)): ?>
									<span class="green-color">Request in progress</span>

								<?php endif ?>
							</div>

						</div>
					<?php endforeach ?>

					<div class="buttons">
						<button type="submit" class="btn btn-primary ajax-post">Verify</button> &nbsp;
						<a href="<?= $this->createUrl('index') ?>" class="ajax-get" data-scrollto="bottom">Cancel</a>
					</div>

					<?php $this->endWidget()  ?>

				</div>

			</div>
		<?php endif ?>

	</div>
<?php $this->endContent(); ?>