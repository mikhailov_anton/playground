<?php $this->beginContent('/layouts/main') ?>
	<div class="settings-general-index">

		<div class="b-logout">
			<a href="<?= $this->createUrl('/site/logout') ?>">Logout</a>
		</div>

		<div class="b-row js-lng">
			<div class="b-col-1">
				<label for="Users_lang">Language</label>
			</div>
			<div class="b-col-2">
				<?php $this->widget('wm.wLng.wLng') ?>
			</div>
		</div>

		<div class="b-row">
			<div class="b-col-1">
				<label for="Users_reply_pm_stop_words">Email signature</label>
			</div>
			<div class="b-col-2">
				<?php $this->widget('wm.wReplyPm.wReplyPm') ?>
			</div>
		</div>

		<div class="b-row js-oal">
			<div class="b-col-1">
				Page after login
			</div>
			<div class="b-col-2">
				<?php $this->widget('wm.wOpenAfterLogin.wOpenAfterLogin') ?>
			</div>
		</div>

		<div class="b-row js-mpl">
			<div class="b-col-1">
				<label for="Users_uid_profile">My profile link</label>
			</div>
			<div class="b-col-2">
				<?php $this->widget('wm.wMyProfileLink.wMyProfileLink') ?>
			</div>
		</div>

		<div class="b-row">
			<div class="b-col-1">
				Password
			</div>
			<div class="b-col-2">
				<div class="b-row-passw">
					<a href="<?= $this->createUrl('changePassword') ?>" class="ajax-get-modal">change</a>
				</div>
			</div>
		</div>

	</div>
<?php $this->endContent() ?>