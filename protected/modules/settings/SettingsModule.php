<?php

class SettingsModule extends WebModule
{
	public function init()
	{
		parent::init();
		$this->setImport(array(
			'settings.models.*',
			'settings.components.*',
		));
		Yii::setPathOfAlias('wm', dirname(__FILE__).'/components/widgets');
	}
}
