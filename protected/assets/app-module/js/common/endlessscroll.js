function EndlessScroll() {

	this.options = {
		$scrollElement: $(window),
		$contentContainer: $('.js-endless-container'),
		reverse: false,
		nextPageUrl: false,
		showProgressBar: false,
		hideProgressBar: false
	};
	this.block = false;
	this.nativeProgressBar;
	this.nativeProgressBarClass = 'b-endless-native-progress';
	this.nativeProgressBarIconClass = 'fa fa-refresh fa-spin';

	var self = this;

	this.init = function(options)
	{
		appUtils.extend(this.options, options);

		this.nativeProgressBar = $('<div/>', {
			class: this.nativeProgressBarClass,
			html: $('<i/>', {class: this.nativeProgressBarIconClass})
		});

		this.options.$scrollElement.on('scroll', function() {
			if (self.options.reverse) {
				if ((self.options.$scrollElement.scrollTop() == 0) && !self.block && self.options.nextPageUrl) {
					self.exec();
				}
			} else {
				if ((self.options.$scrollElement.height()+self.options.$scrollElement.scrollTop() >= self.options.$scrollElement[0].scrollHeight-10) && !self.block && self.options.nextPageUrl) {
					self.exec();
				}
			}
		});
	}

	this.exec = function()
	{
		self.block = true;
		self.showProgressBar();
		appMain.ajaxLoadUrl(self.options.nextPageUrl);
	}

	this.callback = function(options)
	{
		if (this.options.reverse) {
			var scrollHeightBefore = self.options.$scrollElement[0].scrollHeight;
			this.options.$contentContainer.prepend(options.content);
			var scrollDownVal = self.options.$scrollElement[0].scrollHeight - scrollHeightBefore;
			this.options.$scrollElement.scrollTop(scrollDownVal);
		} else {
			this.options.$contentContainer.append(options.content);
		}
		this.options.nextPageUrl = options.nextPageUrl;
		this.block = false;
		this.hideProgressBar();
	}

	this.showProgressBar = function()
	{
		if (this.options.showProgressBar) {
			this.options.showProgressBar();
		} else {
			if (this.options.reverse) {
				this.options.$contentContainer.prepend(this.nativeProgressBar);
			} else {
				this.options.$contentContainer.append(this.nativeProgressBar);
			}
		}
	}

	this.hideProgressBar = function()
	{
		if (this.options.hideProgressBar) {
			this.options.hideProgressBar();
		} else {
			this.options.$contentContainer.find('.'+this.nativeProgressBarClass).remove();
		}
	}
}