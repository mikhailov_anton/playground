module.exports = function (grunt, options) {

	var libsMapCss = {
		bootstrap:		['bootstrap/css/bootstrap.css'],
		bootstrapmin:	['bootstrap/css/typography/bootstrap.css'],
		jAlerts:		['jAlerts/jquery.alerts.css'],
		toastmessage:	['jquery.toastmessage/css/jquery.toastmessage.css'],
		chosen:			['chosen/chosen.css'],
		datetimepicker:	['bootstrap-datetimepicker/css/bootstrap-datetimepicker.css'],
		datepicker:		['bootstrap-datepicker/css/bootstrap-datepicker.css'],
		flags:			['flags/flags.css'],
		flagIcons:		['flag-icons/css/flag-icon.css'],
		jvectormap:		['jvectormap-1.2.2/jquery-jvectormap.css'],
		fineuploader:	['fineuploader/css/fineuploader.css'],
		glyphicons:		['glyphicons/style.css'],
		switcher:		['switcher/css/switcher.css'],
		flexslider:		['flexslider/css/flexslider.css'],
		animate:		['animate/animate.css'],
		fontAwesome:	['font-awesome-4.7.0/css/font-awesome.css'],
		starRating:		['bootstrap-star-rating/css/star-rating.css'],
		checkbox:		['bootstrap-checkbox/bootstrap-checkbox.css'],
		imperavi:		['imperavi-redactor/redactor.css'],
		tutorialize:	['tutorialize/css/tutorialize.css'],
		scrollbar:		['jquery.scrollbar/jquery.scrollbar.css'],
		codehighlight:	['code_highlight/styles/tomorrow.css'],
		bootstrapslider:['slider/css/slider.css'],
		fontello:		[
			'fontello/css/udimi.css',
			'fontello/css/animation.css'
		],
		colorpicker:['bootstrap-colorpicker/dist/css/bootstrap-colorpicker.css'],
		normalize:['normalize/normalize.css'],
		multiplyselect:['multiply_select/multiply_select.css'],
	};

	var packages = {
		memberarea: [
			'bootstrap',
			'jAlerts',
			'toastmessage',
			'chosen',
			'datetimepicker',
			'flags',
			'flagIcons',
			'jvectormap',
			'fineuploader',
			'glyphicons',
			'switcher',
			'flexslider',
			'animate',
			'fontAwesome',
			'starRating',
			'checkbox',
			'imperavi',
			'tutorialize',
			'scrollbar',
			'codehighlight',
			'bootstrapslider',
			'fontello',
			'colorpicker',
			'multiplyselect'
		],
		guestarea: [
			'bootstrap',
			'jAlerts',
			'toastmessage',
			'flags',
			'flagIcons',
			'jvectormap',
			'chosen',
			'datetimepicker',
			'switcher',
			'flexslider',
			'fontAwesome',
			'starRating',
			'checkbox',
			'bootstrapslider',
			'fontello'
		]
	};


	// Do not edit bellow code
	//////////////////////////
	function _buildFiles() {
		var result = {};
		function _getSrc(name) {
			var result = [];
			packages[name].forEach(function(item) {
				libsMapCss[item].forEach(function(filepath) {
					result.push('../httpdocs/media/libs/' + filepath);
				});
			});
			return result;
		}
		for (package in packages) {
			result['runtime/css-compiled/'+package+'-libs.css'] = _getSrc(package);
		}
		return result;
	}

	return {
		styles: {
			options: {staticRoot: '../httpdocs/media/libs/../'},
			files: _buildFiles()
		}
	}
};