(function ($) {

	var methods = {
		init: function (options) {

			return this.each(function () {

				var $this = $(this),
					data = $this.data('dropbugo'),
					$toggle = $this.find('.dropdown-toggle'),
					$menu = $this.find('.dropdown-menu'),
					$menuItem = $menu.find('li a, li span');

				// if plugin does not init yet
				if (!data) {
					$toggle.bind('mouseenter.dropbugo', function(e) {
						var $th = $(this),
							data = $this.data('dropbugo');

						data.menu.show();
					});

					$this.bind('mouseleave.dropbugo', function(e) {
						var $th = $(this),
							data = $this.data('dropbugo');

						data.menu.hide();
					});

					$menuItem.bind('click.dropbugo', function(e) {
						var $th = $(this),
							data = $this.data('dropbugo');

						data.menu.hide();
					});

					$(this).data('dropbugo', {
						target: $this,
						toggle: $toggle,
						menu: $menu
					});

				}
			});
		},
	};

	$.fn.dropbugo = function (method) {
		if (methods[method]) {
			return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
		} else if (typeof method === 'object' || !method) {
			return methods.init.apply(this, arguments);
		} else {
			$.error('Method whith name ' + method + ' does not exist');
		}

	};

})
(jQuery);